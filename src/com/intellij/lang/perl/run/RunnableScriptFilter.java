package com.intellij.lang.perl.run;

import com.intellij.openapi.extensions.ExtensionPointName;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;

/**
 * @author Anna Bulenkova
 */
public interface RunnableScriptFilter {
    ExtensionPointName<RunnableScriptFilter> EP_NAME = ExtensionPointName.create("Perl.runnableScriptFilter");

    boolean isRunnableScript(@NotNull final PsiFile script);
}
